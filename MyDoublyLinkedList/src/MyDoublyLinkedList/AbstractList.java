/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyDoublyLinkedList;

public abstract class AbstractList {
    abstract int size();
    abstract void addFirst(Node o);
    abstract void addLast(Node o);
    abstract void removeFirst();
    abstract void removeLast();
    abstract Node getFirst();
    abstract Node getLast();
    abstract Node get(int index);
    abstract boolean isEmpty();
    abstract boolean isNotEmpty();
}
