/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyDoublyLinkedList;

import java.util.Iterator;

public class DoublyLinkedList extends AbstractList implements Iterator {
    
    private int size = 0;
    private Node first;
    private Node last;
    private Node current;
    
    public DoublyLinkedList() {
        first = null;
        last = null;
    }

    @Override
    int size() {
        return size;
    }

    @Override
    void addFirst(Node o) {
        this.size++;
        Node newNode = new Node(o.getValue());
        if(isEmpty()) {
            last = newNode;
        }
        else {
            first.setPrevious(newNode);
        }
        newNode.setNext(first);
        first = newNode;
    }    

    @Override
    void addLast(Node o) {
        this.size++;
        Node newNode = new Node(o.getValue());
        if(isEmpty()) {
            first = newNode;
        }
        else {
            last.setNext(newNode);
            newNode.setPrevious(last);
        }
        last = newNode;
    }

    @Override
    void removeFirst() {
        if(size != 0) {
            this.size--;
            if(first.getNext() == null) {
                last = null;
            }
            else {
                first.getNext().setPrevious(null);
            }
                first = first.getNext();
        }
    }

    @Override
    void removeLast() {
        if(size != 0) {
            this.size--;
            if(first.getNext() == null) {
                first = null;
            }
            else {
                last.getPrevious().setNext(null);
            }
                last = last.getPrevious();
        }
    }

    @Override
    Node getFirst() {
        return this.first;
    }

    @Override
    Node getLast() {
        return this.last;
    }

    @Override
    Node get(int index) {
        Node a = this.getFirst();
        int i = 1;
        while(a != null) {
            if(i == index)
            return a;
          
            a = a.getNext();
            i++;
        }
        return a;
    }

    @Override
    boolean isEmpty() {
        return first == null;
    }

    @Override
    boolean isNotEmpty() {
        return first != null;
    }

    @Override
    public boolean hasNext() {
        if(current != null) {
            return this.current.getNext() != null;
        }
        
        return this.size != 0;
    }

    @Override
    public Node next() {
        if(current == null) {
            return this.current = this.first;
        }
        
        return this.current = current.getNext();
    }
    
    @Override
    public void remove() {
        this.removeLast();
    }
    
    public void printWholeList() { 
        while(hasNext()) {
            System.out.print(next().getValue() + " ");
        }
        System.out.println();
        this.current = null;
    }

}
