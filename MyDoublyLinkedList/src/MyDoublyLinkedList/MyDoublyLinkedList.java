/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MyDoublyLinkedList;

public class MyDoublyLinkedList {

    public static void main(String[] args) {
        DoublyLinkedList myList = new DoublyLinkedList();
        Node A = new Node(33);
        Node B = new Node(13);
        myList.addFirst(A);
        myList.addFirst(new Node(44));
        myList.addLast(B);
        System.out.println(myList.getFirst().getValue());
        myList.printWholeList();
        System.out.println(myList.get(2).getValue());
        System.out.println("YOLO!");
        System.out.println(myList.size());
        myList.removeLast();
        myList.printWholeList();
        System.out.println(myList.size());
    }
    
}
